# hubot-discord

[![](https://images.microbadger.com/badges/image/laninabox/hubot-discord.svg)](https://microbadger.com/images/laninabox/hubot-discord "Get your own image badge on microbadger.com")
[![](https://images.microbadger.com/badges/version/laninabox/hubot-discord.svg)](https://microbadger.com/images/laninabox/hubot-discord "Get your own version badge on microbadger.com")

A docker image using the [hubot-discord](https://github.com/thetimpanist/hubot-discord) adapter for [hubot](https://hubot.github.com/).

## Usage

When you just run the container, hubot will be started with the discord adapter and the name `plompbot`:

    docker run --rm -ti laninabox/hubot-discord

There are some variables you need to pass to make it work though:

| Variable            | Required | Description |
| ------------------- | -------- | ----------- |
| HUBOT_DISCORD_TOKEN |   yes    | Sets the oauth2 token, generate one [here](https://discordapp.com/developers/applications/me). Create a new application, and then a new bot application. Use that token. |
| HUBOT_MAX_MESSAGE_LENGTH | no | Sets the message length. (later to be deprecated) |
| other_hubot_vars | no | All other HUBOT_VARS, some explanation can be found [here](https://github.com/github/hubot/blob/master/docs/scripting.md#environment-variables).  |


Below is an example with the required ENV variables:

    docker run --rm -ti -e HUBOT_DISCORD_TOKEN=1234567890 laninabox/hubot-discord

You can put these ENV vars in a file for convenience and then run as follows:

    docker run --rm -ti --env-file ./env.list laninabox/hubot-discord

You can add your custom coffeescript like so:

    docker run --rm -ti \
               --env-file ./env.list \
               -v src/custom.coffee:/hubot/scripts/custom.coffee \
               laninabox/hubot-discord

Your custom commands will then be available to hubot.  
Call him by his name.

## Adding a private bot to a discord server

If you selected the bot to be private, you first need to add it to your discord server so the bot is able to join.

To do this, follow this link and replace your `client_id` with your bot's oauth2 token:

    https://discordapp.com/oauth2/authorize?client_id={{bot_oath2_token}}&scope=bot&permissions=0

## Troubleshooting

Run in debug mode and start a custom adapter:

    docker run --rm -ti --env-file ./env.list -e HUBOT_IRC_DEBUG=yes -e HUBOT_LOG_LEVEL="debug" laninabox/hubot -a discord

Or with a shell.

Run in debug mode and start bash:

    docker run --rm -ti --env-file ./env.list -e HUBOT_IRC_DEBUG=yes -e HUBOT_LOG_LEVEL="debug" laninabox/hubot /bin/bash 

From there you can start hubot with:

    /hubot/bin/hubot -a discord --name towerbot --alias jos

All scripts are located in `/hubot/npm_modules/`


## Building

To build this image, just clone the repo and:

    docker build -t laninabox/hubot-discord . 



(c) 2016 Utopit VOF
